package com.epam.training.msa.testing.otpservice.repository

import com.epam.training.msa.testing.otpservice.model.Otp

interface OtpStorage {

    fun getOtpByPersonId(personId: Int): Otp
    fun storeOtpByUserId(otp: Otp): Otp
    fun removeOtpByPersonId(personId: Int)
}