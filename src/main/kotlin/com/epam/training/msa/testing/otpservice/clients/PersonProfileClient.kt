package com.epam.training.msa.testing.otpservice.clients

import org.springframework.cloud.openfeign.FeignClient
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod

/**
 * A client to interact with person-profile-service
 */
@FeignClient(name = "personProfileClient", url = "\${service.person-profile.url}")
interface PersonProfileClient {

    /**
     * gets person's phone number by [personId]
     */
    @RequestMapping(method = [RequestMethod.GET], value = ["/person/{personId}/phone"])
    fun getUserPhone(@PathVariable personId: Int): String

}