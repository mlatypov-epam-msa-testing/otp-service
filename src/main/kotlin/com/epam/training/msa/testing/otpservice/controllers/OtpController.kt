package com.epam.training.msa.testing.otpservice.controllers

import com.epam.training.msa.testing.otpservice.services.OtpService
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/otp")
class OtpController (
        private val otpService: OtpService
) {

    @PostMapping("/send/{personId}")
    fun sendOtp(@PathVariable personId: Int) {
        otpService.sendOtpCode(personId)
    }

    @PostMapping("/check/{personId}")
    fun checkOtp(@PathVariable personId: Int, @RequestBody otp: String): Boolean {
        return otpService.checkOtpCode(personId = personId, otp = otp)
    }
}