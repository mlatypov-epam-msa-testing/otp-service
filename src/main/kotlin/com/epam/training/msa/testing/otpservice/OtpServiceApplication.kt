package com.epam.training.msa.testing.otpservice

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.cloud.openfeign.EnableFeignClients

@SpringBootApplication
@EnableFeignClients
class OtpServiceApplication

fun main(args: Array<String>) {
    runApplication<OtpServiceApplication>(*args)
}
