package com.epam.training.msa.testing.otpservice.services

import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.context.annotation.Primary
import org.springframework.stereotype.Component
import java.lang.StringBuilder
import java.util.concurrent.ThreadLocalRandom
import kotlin.math.pow

@Component
//@ConditionalOnProperty(prefix = "generator", name = ["default-value"], matchIfMissing = true)
//@ConditionalOnMissingBean(value = [OtpGeneratorService::class])
//@Primary
@ConditionalOnProperty(prefix = "generator", name = ["default-value"], havingValue = "false", matchIfMissing = true)
class OtpGeneratorServiceImpl : OtpGeneratorService {

    override fun generateCode(): String {
        val code = ThreadLocalRandom.current().nextInt(1, 10.0.pow(LENGTH.toDouble()).toInt()).toString()
        return if (code.length == 4) {
            code
        } else {
            return StringBuilder(code).apply {
                for (i in 1..(LENGTH - code.length)) {
                    append(i)
                }
            }.toString()
        }
    }

    companion object {
        private const val LENGTH = 4
    }
}