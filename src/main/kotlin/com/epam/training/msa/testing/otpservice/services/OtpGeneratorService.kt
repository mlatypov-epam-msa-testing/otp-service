package com.epam.training.msa.testing.otpservice.services

/**
 * One-time password generation service
 */
interface OtpGeneratorService {

    /**
     * Generation of an one-time password
     */
    fun generateCode(): String

}