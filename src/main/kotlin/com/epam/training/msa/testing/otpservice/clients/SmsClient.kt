package com.epam.training.msa.testing.otpservice.clients

import org.springframework.cloud.openfeign.FeignClient
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod

/**
 * Клиент для отправки СМС с помощью сервиса отправки СМС
 */
@FeignClient(name = "smsClient", url = "\${service.sms.url}")
interface SmsClient {

    /**
     * sends sms-message with [text] to [phone]
     */
    @RequestMapping(method = [RequestMethod.POST], value = ["/send/{phone}"])
    fun sendSms(@PathVariable phone: String, @RequestBody text: String)

}