package com.epam.training.msa.testing.otpservice.exceptions

import java.lang.RuntimeException

class OtpDoesNotExistException : RuntimeException() {

}
