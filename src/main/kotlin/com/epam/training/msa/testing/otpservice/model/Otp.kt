package com.epam.training.msa.testing.otpservice.model

import java.time.LocalDateTime

data class Otp(
        val personId: Int,
        val otpCode: String,
        val creationDate: LocalDateTime = LocalDateTime.now()
)