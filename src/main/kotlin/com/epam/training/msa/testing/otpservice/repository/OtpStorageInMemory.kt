package com.epam.training.msa.testing.otpservice.repository

import com.epam.training.msa.testing.otpservice.exceptions.OtpAlreadyExistsException
import com.epam.training.msa.testing.otpservice.exceptions.OtpDoesNotExistException
import com.epam.training.msa.testing.otpservice.model.Otp
import org.springframework.stereotype.Component
import java.util.concurrent.ConcurrentHashMap

@Component
class OtpStorageInMemory : OtpStorage {

    private val storageMap: MutableMap<Int, Otp> = ConcurrentHashMap()

    override fun storeOtpByUserId(otp: Otp): Otp {
        if (!storageMap.containsKey(otp.personId)) {
            storageMap[otp.personId] = otp
            return otp
        } else
            throw OtpAlreadyExistsException()
    }

    override fun removeOtpByPersonId(personId: Int) {
        if (storageMap.containsKey(personId))
            storageMap.remove(personId)
        else
            throw OtpDoesNotExistException()
    }

    override fun getOtpByPersonId(personId: Int): Otp {
        if (storageMap.containsKey(personId))
            return storageMap[personId]!!
        else
            throw OtpDoesNotExistException()
    }
}