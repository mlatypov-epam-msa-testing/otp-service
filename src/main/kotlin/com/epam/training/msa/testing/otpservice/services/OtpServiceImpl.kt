package com.epam.training.msa.testing.otpservice.services

import com.epam.training.msa.testing.otpservice.clients.PersonProfileClient
import com.epam.training.msa.testing.otpservice.clients.SmsClient
import com.epam.training.msa.testing.otpservice.model.Otp
import com.epam.training.msa.testing.otpservice.repository.OtpStorage
import org.springframework.stereotype.Component
import java.time.LocalDateTime

@Component
class OtpServiceImpl(
        val otpGeneratorService: OtpGeneratorService,
        val otpStorage: OtpStorage,
        val personProfileClient: PersonProfileClient,
        val smsClient: SmsClient
): OtpService {

    override fun sendOtpCode(personId: Int) {
        val phone = personProfileClient.getUserPhone(personId)
        val code = otpGeneratorService.generateCode()
        val text = "Please, enter the code: $code"
        val otp = Otp(personId = personId, otpCode = code, creationDate = LocalDateTime.now())
        otpStorage.storeOtpByUserId(otp)
        smsClient.sendSms(phone, text)
    }

    override fun checkOtpCode(personId: Int, otp: String): Boolean {
        return kotlin.runCatching {
            val result = otpStorage.getOtpByPersonId(personId).otpCode == otp
            if (result)
                otpStorage.removeOtpByPersonId(personId)
            result
        }.getOrDefault(false)
    }
}