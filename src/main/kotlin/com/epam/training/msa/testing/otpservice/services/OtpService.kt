package com.epam.training.msa.testing.otpservice.services

interface OtpService {

    /**
     * Generates OTP, saves it in cache and send it to user's phone with specific [personId]
     */
    fun sendOtpCode(personId: Int)

    /**
     * checks [otp] by [personId]
     */
    fun checkOtpCode(personId: Int, otp: String): Boolean

}