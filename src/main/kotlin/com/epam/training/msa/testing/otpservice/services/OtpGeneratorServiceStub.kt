package com.epam.training.msa.testing.otpservice.services

import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.stereotype.Component
import javax.annotation.PostConstruct

@ConditionalOnProperty(prefix = "generator", name = ["default-value"], matchIfMissing = false)
@Component
class OtpGeneratorServiceStub(@Value("\${generator.default-value}") val defaultValue: String) : OtpGeneratorService {

    val logger = LoggerFactory.getLogger(javaClass)

    @PostConstruct
    fun init() {
        logger.warn("loaded test generator with default code: {}", defaultValue)
    }

    override fun generateCode(): String {
        return defaultValue
    }
}