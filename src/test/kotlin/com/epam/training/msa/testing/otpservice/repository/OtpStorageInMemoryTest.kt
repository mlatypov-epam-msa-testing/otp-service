package com.epam.training.msa.testing.otpservice.repository

import com.epam.training.msa.testing.otpservice.exceptions.OtpAlreadyExistsException
import com.epam.training.msa.testing.otpservice.exceptions.OtpDoesNotExistException
import com.epam.training.msa.testing.otpservice.model.Otp
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import java.time.LocalDateTime

internal class OtpStorageInMemoryTest {

    private val otpStorage: OtpStorage = OtpStorageInMemory()


    @Test
    fun testStorage() {
        val otp = Otp(personId = 1, otpCode = "12345", creationDate = LocalDateTime.now())
        assertThat(otpStorage.storeOtpByUserId(otp)).isEqualTo(otp)
        assertThrows<OtpAlreadyExistsException> { otpStorage.storeOtpByUserId(otp) }
        assertThat(otpStorage.getOtpByPersonId(1)).isEqualTo(otp)
        assertThrows<OtpDoesNotExistException> {
            otpStorage.removeOtpByPersonId(1)
            otpStorage.getOtpByPersonId(1)
        }
    }
    
    


}