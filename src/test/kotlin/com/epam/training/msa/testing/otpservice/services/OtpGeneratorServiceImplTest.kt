package com.epam.training.msa.testing.otpservice.services

import org.junit.jupiter.api.Assertions.*
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.RepeatedTest
import org.junit.jupiter.api.Test

internal class OtpGeneratorServiceImplTest {

    private val otpGeneratorService = OtpGeneratorServiceImpl()

    @Test
    @RepeatedTest(10)
    fun testGenerator() {
        assertThat(otpGeneratorService.generateCode()).isNotEqualTo(otpGeneratorService.generateCode())
        assertThat(otpGeneratorService.generateCode().length).isEqualTo(4)
    }

}