package com.epam.training.msa.testing.otpservice.services

import com.epam.training.msa.testing.otpservice.clients.PersonProfileClient
import com.epam.training.msa.testing.otpservice.clients.SmsClient
import com.epam.training.msa.testing.otpservice.model.Otp
import com.epam.training.msa.testing.otpservice.repository.OtpStorage
import io.mockk.every
import io.mockk.mockk
import io.mockk.mockkStatic
import io.mockk.slot
import io.mockk.verify
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
internal class OtpServiceImplTest {
    val otpGeneratorService = mockk<OtpGeneratorService>()
    val otpStorage = mockk<OtpStorage>(relaxed = true)
    val smsClient = mockk<SmsClient>(relaxed = true)
    val personProfileClient = mockk<PersonProfileClient>(relaxed = true)
    private val otpService = OtpServiceImpl(
            otpGeneratorService = otpGeneratorService,
            otpStorage = otpStorage,
            smsClient = smsClient,
            personProfileClient = personProfileClient
    )

    @Test
    fun testSendOtpCode() {
        every {
            otpGeneratorService.generateCode()
        } returns "12345"

        every {
            personProfileClient.getUserPhone(1)
        } returns "+79998887654"

        otpService.sendOtpCode(1)

        verify {
            smsClient.sendSms(
                    "+79998887654",
                    "Please, enter the code: 12345"
            )
        }

        val otpSlot = slot<Otp>()
        verify(exactly = 1) {
            otpStorage.storeOtpByUserId(capture(otpSlot))
        }
        assertThat(otpSlot.captured).isNotNull
        assertThat(otpSlot.captured.otpCode).isEqualTo("12345")
        assertThat(otpSlot.captured.personId).isEqualTo(1)
    }

    @Test
    fun testCheckOtpCode() {
        every {
          otpStorage.getOtpByPersonId(1)
        } returns Otp(personId = 1, otpCode = "12345")

        assertThat(otpService.checkOtpCode(1, "54321"))
                .isFalse()

        verify(exactly = 0) {
            otpStorage.removeOtpByPersonId(1)
        }

        assertThat(otpService.checkOtpCode(1, "12345"))
                .isTrue()

        verify(exactly = 1) {
            otpStorage.removeOtpByPersonId(1)
        }
    }

    @Test
    fun testExtension() {

        mockkStatic("com.epam.training.msa.testing.otpservice.services.OtpServiceExtensionsKt")


        every {
            otpService.otpIsActive(1)
        } returns true

        every {
            otpService.otpIsActive(2)
        } returns false

        assertThat(otpService.otpIsActive(1)).isTrue()
        assertThat(otpService.otpIsActive(2)).isFalse()
    }

}

