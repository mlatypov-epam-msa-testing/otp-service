FROM openjdk:8-jdk-alpine

ARG APP_NAME
EXPOSE 8000

COPY target/$APP_NAME /opt/app.jar
CMD java -jar /opt/app.jar